import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {DefaultComponent} from "./layouts/default/default.component";
import {AdminComponent} from "./modules/admins/admin/admin.component";
import {HomeComponent} from "./modules/public/home/home.component";
import {LoginComponent} from "./modules/public/security/login/login.component";
import {LogoutComponent} from "./modules/public/security/logout/logout.component";
import {AuthGaurdService} from "./modules/public/security/service/auth-gaurd.service";
import {ProductOverviewComponent} from "./modules/admins/product-overview/product-overview.component";

const routes: Routes = [
  {path: '', component: HomeComponent},
  { path: 'login', component: LoginComponent },
  { path: 'logout', component: LogoutComponent,canActivate:[AuthGaurdService] },
  {path: 'admin', component: DefaultComponent,canActivate:[AuthGaurdService],
    children: [{
      path: '',
      component: AdminComponent
    },
      {
      path: 'products',
      component: ProductOverviewComponent
    }]
  }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
