import { Injectable } from '@angular/core';
import {MatConfirmDialogComponent} from './mat-confirm-dialog.component';
import {MatDialog} from "@angular/material/dialog";

@Injectable({
  providedIn: 'root'
})
export class DialogConfirmService {

  constructor(private dialog: MatDialog) { }

  openconfirmDialog(msg: any) {
    return this.dialog.open(MatConfirmDialogComponent, {
      width: '390px',
      panelClass: 'confirm-dialog-container',
      disableClose: true,
      position: { top: '10%'},
      data: {
       message: msg
      }
    });
  }
}
