import { Injectable } from '@angular/core';
import {MatSnackBar, MatSnackBarConfig} from "@angular/material/snack-bar";

@Injectable({
  providedIn: 'root'
})
export class NotificationFlashService {

  constructor(private snackBar: MatSnackBar) { }
  config: MatSnackBarConfig = {
    duration: 5000,
    horizontalPosition: 'right',
    verticalPosition: 'top'
  }
  success(msg: any) {
    this.config.panelClass = ['notification-success'];
    this.snackBar.open(msg, '', this.config);
  }

  errorFlash(msg: any) {
    this.config.panelClass = ['notification-error'];
    this.snackBar.open(msg, '', this.config);
  }
}
