import {Component, Inject, LOCALE_ID, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {ProductService} from "../product.service";
import {NotificationFlashService} from "../../../../shared/notification-flash.service";

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.scss']
})
export class ProductFormComponent implements OnInit {


  constructor(private dialogRef: MatDialogRef<ProductFormComponent>,
              @Inject(LOCALE_ID) private locale: string,
              @Inject(MAT_DIALOG_DATA) public data: any,
              public productService: ProductService,
              private notifService: NotificationFlashService) { }

  ngOnInit(): void {
  }

  onSubmit() {
    if (this.productService.form.valid) {
      // @ts-ignore
      if (!this.productService.form.get('id').value) {
        this.addProdct();
      } else {
        this.editProduct();
        this.notifService.success('La modification a été prise en compte');
      }
      this.productService.form.reset();
      this.productService.initializeFormGroupe();
      this.onClose();
    }
  }

  private addProdct() {
    this.productService.addProduct(this.productService.form.value).subscribe(
        data => {
          this.notifService.success('A new Product has been created');
          this.onClose();
        },
          error => {
            this.notifService.errorFlash(error.error.message);
          })
  }
  onClose() {
    this.productService.form.reset();
    this.productService.initializeFormGroupe();
    this.dialogRef.close();
  }

  private editProduct() {
    // @ts-ignore
    this.productService.updateProduct(this.productService.form.value, this.productService.form.get('id').value).subscribe(
      data => {
        this.notifService.success('The product has been modified with success');
        this.onClose();
      },
      error => {
        this.notifService.errorFlash(error.error.message);
      })
  }

  onClean() {
    this.productService.form.reset();
    this.productService.initializeFormGroupe();
    this.onClose();
  }
}
