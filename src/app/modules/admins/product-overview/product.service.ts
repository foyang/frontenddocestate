import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {FormControl, FormGroup, Validators} from "@angular/forms";

export class Product{
  constructor(
    public name: string,
    public type: string,
    public price: number
  ) {}
}
@Injectable({
  providedIn: 'root'
})
export class ProductService {

   username='docestate'
   password='password'

  constructor(private httpClient:HttpClient) { }

  getProducts() {
    const headers = new HttpHeaders({ Authorization: 'Basic ' + btoa(this.username + ':' + this.password) });
    return this.httpClient.get<Product[]>('http://localhost:9001/products',{headers});
  }
  addProduct(product: Product){
    const headers = new HttpHeaders({ Authorization: 'Basic ' + btoa(this.username + ':' + this.password)});
    headers.set('Content-Type','application/json')
    return this.httpClient.post<Product>('http://localhost:9001/products/add', product,{headers});
  }
  updateProduct(product: Product, id: any){
    const headers = new HttpHeaders({ Authorization: 'Basic ' + btoa(this.username + ':' + this.password) });
    return this.httpClient.put<Product>('http://localhost:9001/products/'+id,product, {headers});
  }
  deleteProduct(id: number){
    const headers = new HttpHeaders({ Authorization: 'Basic ' + btoa(this.username + ':' + this.password) });
    return this.httpClient.delete<Product>('http://localhost:9001/products/'+id,{headers, responseType: 'json'});
  }

  form: FormGroup = new FormGroup({
    id: new FormControl(null),
    name: new FormControl('',Validators.required),
    type: new FormControl(''),
    price: new FormControl('')
  });

  initializeFormGroupe() {
    this.form.setValue({
      id: null,
      name:'',
      type: '',
      price: ''
    });
  }
  populatedForm(rowValue: any) {
    this.form.patchValue(rowValue);
  }
}
