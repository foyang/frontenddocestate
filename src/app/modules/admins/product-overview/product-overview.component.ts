import { Component, OnInit } from '@angular/core';
import {ProductService} from "./product.service";
import {MatDialog, MatDialogConfig} from "@angular/material/dialog";
import {ProductFormComponent} from "./product-form/product-form.component";
import {DialogConfirmService} from "../../../shared/components/mat-confirm-dialog/dialog-confirm.service";
import {NotificationFlashService} from "../../../shared/notification-flash.service";

export class Product{
  constructor(
    public name:string,
    public type:string,
    public price:number
  ) {}
}
@Component({
  selector: 'app-product-overview',
  templateUrl: './product-overview.component.html',
  styleUrls: ['./product-overview.component.scss']
})
export class ProductOverviewComponent implements OnInit {

  products: Product[] = [];
  displayedColumns: string[] = ['id', 'name', 'type', 'price','action'];
  dataSource: any;
  constructor(private productService: ProductService,
              private dialog: MatDialog,
              private dialogConfirmService: DialogConfirmService,
              private notifService: NotificationFlashService
  ) { }

  ngOnInit(): void {
    this.getProducts();
  }
  getProducts(){
    this.productService.getProducts().subscribe(
      data =>{
        this.products = data;
        this.dataSource = this.products;
      },
      error => {
        console.log('Error...');
      }
    )
  }

  onCreate() {
    this.productService.initializeFormGroupe();
    const config = new MatDialogConfig();
    config.disableClose = true;
    config.autoFocus = true;
    config.width = '40%';
    config.position = {top: '2%'};
    const dialogOpen = this.dialog.open(ProductFormComponent, config);
    dialogOpen.afterClosed().subscribe( data => {
      setTimeout( () => {
        this.getProducts();
      },500);
    });
  }
  onEdit(rowValue: any) {
    this.productService.populatedForm(rowValue);
    const config = new MatDialogConfig();
    config.disableClose = true;
    config.autoFocus = true;
    config.width = '50%';
    config.position = {top: '10%'};
    const dialogOpen = this.dialog.open(ProductFormComponent, config);
    dialogOpen.afterClosed().subscribe( data => {
      this.notifService.success('! Successful Edited');
      this.getProducts();
    });
  }

  delete(element: any) {
    this.dialogConfirmService.openconfirmDialog('Do you really want to perform this action?')
      .afterClosed().subscribe(res => {
      if (res) {
        this.productService.deleteProduct(element.id).subscribe(
          data => {
            this.getProducts();
            this.notifService.success('! Successful deletion');
          },
          error => {
            console.log('########: Error');
          }
          )
      }
    })
  }
}
