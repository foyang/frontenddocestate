import { Component, OnInit } from '@angular/core';
import {ProductService} from "../../admins/product-overview/product.service";

export class Product{
  constructor(
    public name:string,
    public type:string,
    public price:number
  ) {}
}
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  products: Product[] = [];
  displayedColumns: string[] = ['id', 'name', 'type', 'price'];
  dataSource: any;
  constructor(private productService: ProductService) { }

  ngOnInit(): void {
    this.getProducts();
  }
  getProducts(){
    this.productService.getProducts().subscribe(
      data =>{
        this.products = data;
        this.dataSource = this.products;
      },
      error => {
        console.log('Error...');
      }
    )
  }
}
